// Copyright (C) 2015 by Tapjoy Inc.
//
// This file is part of the Tapjoy SDK.
//
// By using the Tapjoy SDK in your software, you agree to the terms of the Tapjoy SDK License Agreement.
//
// The Tapjoy SDK is bound by the Tapjoy SDK License Agreement and can be found here: https://www.tapjoy.com/sdk/license

#import "MoPubCustomPlacement.h"
#import "MPInterstitialAdController.h"

@interface MoPubCustomPlacement () <MPInterstitialAdControllerDelegate>

@property (nonatomic, retain) MPInterstitialAdController *interstitial;

@end

@implementation MoPubCustomPlacement


- (void)requestContentWithCustomPlacementParams:(NSDictionary *)params
{
    NSString *adUnitID = [params objectForKey:@"adUnitID"];
    _interstitial = [MPInterstitialAdController interstitialAdControllerForAdUnitId:adUnitID];
    _interstitial.delegate = self;
    
    // Load the interstitial ad
    [self.interstitial loadAd];
}

- (void)showContentWithViewController:(UIViewController*)viewController
{
    if (self.interstitial.ready) {
        [self.interstitial showFromViewController:viewController];
    } else {
        // The interstitial wasn't ready, so continue as usual.
    }
}

- (void)dealloc
{
    _interstitial.delegate = nil;
}


#pragma mark - MPInterstitialAdControllerDelegate

- (void)interstitialDidLoadAd:(MPInterstitialAdController *)interstitial
{
    [self.delegate customPlacement:self didLoadAd:interstitial];
}

- (void)interstitialDidFailToLoadAd:(MPInterstitialAdController *)interstitial
{
    NSMutableDictionary* errorDetails = [NSMutableDictionary dictionary];
    [errorDetails setValue:@"Failed to load MoPub interstitial content" forKey:NSLocalizedDescriptionKey];
    NSError *error = [NSError errorWithDomain:@"Tapjoy Placement Medation" code:204 userInfo:errorDetails];
    [self.delegate customPlacement:self didFailWithError:error];
}

- (void)interstitialDidAppear:(MPInterstitialAdController *)interstitial
{
    [self.delegate customPlacementContentDidAppear:self];
}

- (void)interstitialDidDisappear:(MPInterstitialAdController *)interstitial
{
    [self.delegate customPlacementContentDidDisappear:self];
}

@end
