// Copyright (C) 2015 by Tapjoy Inc.
//
// This file is part of the Tapjoy SDK.
//
// By using the Tapjoy SDK in your software, you agree to the terms of the Tapjoy SDK License Agreement.
//
// The Tapjoy SDK is bound by the Tapjoy SDK License Agreement and can be found here: https://www.tapjoy.com/sdk/license

#import "MoPubRewardedVideoCustomPlacement.h"

@interface MoPubRewardedVideoCustomPlacement () <MPRewardedVideoDelegate>

@property (nonatomic, retain) MPInterstitialAdController *interstitial;
@property (nonatomic, retain) NSString *adUnitID;

@end

@implementation MoPubRewardedVideoCustomPlacement

- (void)requestContentWithCustomPlacementParams:(NSDictionary *)params
{
    [[MoPub sharedInstance] initializeRewardedVideoWithGlobalMediationSettings:nil delegate:self];

    _adUnitID = [params objectForKey:@"adUnitID"];
    [MPRewardedVideo loadRewardedVideoAdWithAdUnitID:_adUnitID withMediationSettings:nil];
}

- (void)showContentWithViewController:(UIViewController*)viewController
{
    [MPRewardedVideo presentRewardedVideoAdForAdUnitID:_adUnitID fromViewController:viewController];
}

- (void)dealloc
{
    _interstitial.delegate = nil;
}


- (void)rewardedVideoAdDidLoadForAdUnitID:(NSString *)adUnitID {
    NSLog(@"MoPub rewarded video loaded for ad unit ID %@", adUnitID);
    [self.delegate customPlacement:self didLoadAd:adUnitID];
}

- (void)rewardedVideoAdDidFailToLoadForAdUnitID:(NSString *)adUnitID error:(NSError *)error {
    NSLog(@"MoPub rewarded video ad did fail to load for ad unit ID %@ with error %@", adUnitID, [error localizedDescription]);
    [self.delegate customPlacement:self didFailWithError:error];
}

- (void)rewardedVideoAdDidAppearForAdUnitID:(NSString *)adUnitID {
    NSLog(@"MoPub rewarded video did appear for ad unit ID %@", adUnitID);
    [self.delegate customPlacementContentDidAppear:self];
}

- (void)rewardedVideoAdDidDisappearForAdUnitID:(NSString *)adUnitID {
    NSLog(@"MoPub rewarded video did disappear for ad unit ID %@", adUnitID);
    [self.delegate customPlacementContentDidDisappear:self];
}

- (void)rewardedVideoAdShouldRewardForAdUnitID:(NSString *)adUnitID reward:(MPRewardedVideoReward *)reward {
    NSLog(@"MoPub rewarded video should reward type %@, amount %@, for ad unit ID %@", reward.currencyType, reward.amount, adUnitID);
    [self.delegate customPlacement:self shouldReward:reward.currencyType amount:[reward.amount intValue]];
}

@end

