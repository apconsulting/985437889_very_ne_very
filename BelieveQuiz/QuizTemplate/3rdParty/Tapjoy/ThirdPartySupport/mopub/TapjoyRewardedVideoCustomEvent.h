
#if __has_include(<MoPub/MoPub.h>)
    #import <MoPub/MoPub.h>
#else
    #import "MPRewardedVideoCustomEvent.h"
#endif

/*
 * Certified with version 11.4.0 of the Tapjoy SDK.
 */

@interface TapjoyRewardedVideoCustomEvent : MPRewardedVideoCustomEvent

@end
