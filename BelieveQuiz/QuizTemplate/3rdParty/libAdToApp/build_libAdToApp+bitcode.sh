#!/bin/sh

cd "$( dirname "${BASH_SOURCE[0]}" )"

#temporarily remove providers that is not bitcode compatible
mv ./libProviderAmazon.a ./libProviderAmazon.excluded
mv ./libProviderSmaato.a ./libProviderSmaato.excluded
mv ./libProviderStartApp.a ./libProviderStartApp.excluded
mv ./libProviderAvocarrot.a ./libProviderAvocarrot.excluded
mv ./libProviderInstreamatic.a ./libProviderInstreamatic.excluded
mv ./libProviderNativeX.a ./libProviderNativeX.excluded #supports new bitcode version
mv ./libProviderTapSense.a ./libProviderTapSense.excluded #supports new bitcode version
mv ./libProviderInMobi.a ./libProviderInMobi.excluded #supports new bitcode version
mv ./libProviderFacebook.a ./libProviderFacebook.excluded #supports new bitcode version

rm -rf ./libAdToApp.a
libtool  -static ./*.a -o ./libAdToApp.a

#restore names of all providers
mv ./libProviderAmazon.excluded ./libProviderAmazon.a
mv ./libProviderSmaato.excluded ./libProviderSmaato.a
mv ./libProviderStartApp.excluded ./libProviderStartApp.a
mv ./libProviderAvocarrot.excluded ./libProviderAvocarrot.a
mv ./libProviderInstreamatic.excluded ./libProviderInstreamatic.a
mv ./libProviderNativeX.excluded ./libProviderNativeX.a #supports new bitcode version
mv ./libProviderTapSense.excluded ./libProviderTapSense.a #supports new bitcode version
mv ./libProviderInMobi.excluded ./libProviderInMobi.a #supports new bitcode version
mv ./libProviderFacebook.excluded ./libProviderFacebook.a #supports new bitcode version

echo "DONE!"