#!/bin/sh

cd "$( dirname "${BASH_SOURCE[0]}" )"

#temporarily remove providers that is not compatible with iOS6
mv ./libProviderSmaato.a ./libProviderSmaato.excluded

rm -rf ./libAdToApp.a
libtool  -static ./*.a -o ./libAdToApp.a

#restore names of all providers
mv ./libProviderSmaato.excluded ./libProviderSmaato.a

echo "DONE!"