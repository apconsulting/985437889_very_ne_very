#import "SAActionHandlerConfig+JSONParser.h"
#import "SBJsonParser.h"


@implementation SAActionHandlerConfig (JSONParser)

- (NSDictionary *)JSONFromString:(NSString *)string {
    SBJsonParser *parser = [SBJsonParser new];
    id result = [parser objectWithString:string];

//    NSLog(@"SBJSON parsing result: %@", result);
    return result;
}

@end