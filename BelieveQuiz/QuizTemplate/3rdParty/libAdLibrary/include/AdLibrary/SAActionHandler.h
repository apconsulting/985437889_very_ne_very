#import <Foundation/Foundation.h>

@protocol SAActionHandler

- (BOOL)handle:(NSString *)actionType request:(NSURLRequest *)request target:(id)target;

@end