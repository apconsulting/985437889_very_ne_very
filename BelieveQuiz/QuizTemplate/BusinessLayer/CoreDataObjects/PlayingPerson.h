//
//  PlayingPerson.h
//  AudioQuiz
//
//  Created by Vladislav on 5/28/13.
//  Copyright (c) 2013 Vladislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Person;
@class PlayingQuestion;

@interface PlayingPerson : NSObject
{
    Person *_person;
    BOOL _isPlayerIsWaiting;
}

@property (nonatomic, readonly) Person *person;

@property (nonatomic, assign) BOOL isPlayerIsWaiting;


+(PlayingPerson*)PlayingPerson;


-(NSDictionary*)calculateResultWithBid:(NSInteger)bid;

-(BOOL)personSpentCoins:(NSInteger)coins;
-(void)personGetCoins:(NSInteger)coins;
-(void)personGetPoints:(NSInteger)points;
-(void)personGetPointsOffline:(NSInteger)points;

-(NSInteger)personLastQuestionIndex:(NSInteger)level;
-(void)encrementQuestionIndex:(NSInteger)level;
-(void)resetQuestionIndex:(NSInteger)level;

-(void)saveBotGameResult:(NSInteger)result;
@end
