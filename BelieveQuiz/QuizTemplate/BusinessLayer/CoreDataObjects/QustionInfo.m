//
//  QustionInfo.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "QustionInfo.h"
#import "QuestionNew.h"


@implementation QustionInfo

@dynamic bIsRightAnswer;
@dynamic index;
@dynamic title;
@dynamic question;
@dynamic bIsRemoved;

- (NSDictionary*)wkDictionary {
    return @{@"bIsRightAnswer":self.bIsRightAnswer, @"index":self.index, @"title":self.title};
}

@end
