//
//  QuestionInitializer.m
//  AudioQuiz
//
//  Created by Vladislav on 4/8/13.
//  Copyright (c) 2013 Vladislav. All rights reserved.
//

#import "QuestionInitializer.h"
#import "QuestionNew.h"
#import "QustionInfo.h"
#import "SBJsonParser.h"
#import "AppSettings.h"
#import "CRFileUtils.h"
#import "OpenedCell.h"
#import "TBXML.h"
#import "const.h"

@implementation QuestionInitializer

+(BOOL)initializeCoreData
{
    TBXML* tbxml = [TBXML tbxmlWithXMLFile:[CRFileUtils resourcePath:@"questions.xml"]];
        
    TBXMLElement *root = tbxml.rootXMLElement;
    if (!root) {
        NSLog(@"Ошибка чтения корня XML");
        return NO;
    }
    
    TBXMLElement *channel = [TBXML childElementNamed:@"Questions" parentElement:root];
    if (channel) {
        int iRow = 0;
        TBXMLElement *item = [TBXML childElementNamed:@"row" parentElement:channel];
        while (item) {
            
            TBXMLElement *Question = [TBXML childElementNamed:@"Question" parentElement:item];
            
            TBXMLElement *cell1 = [TBXML childElementNamed:@"value" parentElement:item];
            TBXMLElement *cell2 = [TBXML childElementNamed:@"Answer" parentElement:item];
            //            TBXMLElement *cell3 = [TBXML childElementNamed:@"cell3" parentElement:item];
            //            TBXMLElement *cell4 = [TBXML childElementNamed:@"cell4" parentElement:item];
            
            NSString* strValue = [TBXML textForElement: cell1];
            BOOL isRight = [strValue isEqualToString:@"1"];
            
            NSLog(@"CELL1: %@  %@",[TBXML textForElement:cell1] , isRight?@"YES": @"NO");
            iRow++;
            
            QuestionNew* question = (QuestionNew*)[QuestionNew createObject];
            question.questionId = [NSNumber numberWithInteger:iRow];
            question.questionRemoveAllCells = [NSNumber numberWithBool:NO];
            question.questionText = [TBXML textForElement:Question];
            question.answer = [TBXML textForElement:cell2];
            //temp Opened Cell
            OpenedCell *op = (OpenedCell*)[OpenedCell createObject];
            op.cellX = [NSNumber numberWithInt:200];
            op.cellY = [NSNumber numberWithInt:200];
            op.question = question;
            
            
            
            QustionInfo *answer1 = (QustionInfo*)[QustionInfo createObject];
            answer1.index = [NSNumber numberWithInt:-1];
            answer1.bIsRightAnswer = [NSNumber numberWithBool:isRight];
            answer1.title = [TBXML textForElement:cell2];
            
            answer1.bIsRemoved = [NSNumber numberWithBool:NO];
            answer1.question = question;
            
            
            QustionInfo *answer2 = (QustionInfo*)[QustionInfo createObject];
            answer2.index = [NSNumber numberWithInt:0];
            answer2.bIsRightAnswer = [NSNumber numberWithBool:isRight];
            answer2.title = [TBXML textForElement:cell2];
            answer2.bIsRemoved = [NSNumber numberWithBool:NO];
            answer2.question = question;
            
            item = [TBXML nextSiblingNamed:@"row" searchFromElement:item];
        }
    }
        
    
    return YES;
    
}

@end
