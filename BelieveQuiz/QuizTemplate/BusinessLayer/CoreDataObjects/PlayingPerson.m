//
//  PlayingPerson.m
//  AudioQuiz
//
//  Created by Vladislav on 5/28/13.
//  Copyright (c) 2013 Vladislav. All rights reserved.
//

#import "PlayingPerson.h"
#import "Person.h"
#import "PlayingQuestion.h"
#import <Tapjoy/Tapjoy.h>
#import "GameCenterManager.h"
#import "GameModel.h"


@implementation PlayingPerson
@synthesize person = _person;
@synthesize isPlayerIsWaiting = _isPlayerIsWaiting;

-(id)initWithPerson:(Person*)person
{
    if ((self = [super init]) !=nil)
    {
        _person = person;
        
    }
    
    return self;
}

+(PlayingPerson*)PlayingPerson
{
    Person *person = [[Person allObjects] lastObject];
    return [[PlayingPerson alloc] initWithPerson:person];
}





-(NSInteger)personLastQuestionIndex:(NSInteger)level
{
    switch (level) {
        case 2:
            return [_person.questionIndex2 integerValue];
            
            break;
        case 3:
            return [_person.questionIndex3 integerValue];
            
        default:
            break;
    }
    return [_person.questionIndex1 integerValue];
}

-(void)encrementQuestionIndex:(NSInteger)level
{
    switch (level) {
        case 2:
            _person.questionIndex2 = [NSNumber numberWithInteger:[_person.questionIndex2 integerValue] + 1];
            break;
        case 3:
            _person.questionIndex3 = [NSNumber numberWithInteger:[_person.questionIndex3 integerValue] + 1];
            break;
        default:
            _person.questionIndex1 = [NSNumber numberWithInteger:[_person.questionIndex1 integerValue] + 1];
            break;
    }
    [DELEGATE saveContext];
}

-(void)resetQuestionIndex:(NSInteger)level {
    switch (level) {
        case 2:
            _person.questionIndex2 = 0;
            break;
        case 3:
            _person.questionIndex3 = 0;
            break;
        default:
            _person.questionIndex1 = 0;
            break;
    }
    [DELEGATE saveContext];
}

-(NSDictionary*)calculateResultWithBid:(NSInteger)bid
{
    NSInteger myCoins = 0;
    NSInteger myPoints = 0;
    
    NSInteger myRights = 0;
    NSInteger opRights = 0;
    NSInteger myTotalTime = 0;
    NSInteger opTotalTime = 0;
    
    NSDictionary *myQuestions = [GameModel sharedInstance].myPlayingQuestions;
    NSDictionary *opQuestions = [GameModel sharedInstance].opponentPlayingQuestions;
    
    for (int i = 0; i < 5; i++)
    {
        PlayingQuestion *myPlQ = [myQuestions objectForKey:[NSNumber numberWithInt:i]];
        if (!myPlQ)
        {
            myPlQ = [PlayingQuestion playingQuestionWithId:i answerTime:[[GameModel sharedInstance] questionTime] isRight:NO];
        }
        
        PlayingQuestion *opPlQ = [opQuestions objectForKey:[NSNumber numberWithInt:i]];
        if (!opPlQ)
        {
            opPlQ = [PlayingQuestion playingQuestionWithId:i answerTime:[[GameModel sharedInstance] questionTime] isRight:NO];
        }
        
        //        NSLog(@"For Quiestion - %d\n MyTime - %d Right - %@\n OpTime - %d Right - %@", i+1, myPlQ.questionTime, myPlQ.bIsRightAnswer ? @"YES" : @"NO", opPlQ.questionTime, opPlQ.bIsRightAnswer ? @"YES" : @"NO");
        
        myTotalTime += myPlQ.questionTime;
        opTotalTime += opPlQ.questionTime;
        
        if (myPlQ.bIsRightAnswer)
        {
            myRights++;
            myPoints += 5;
        }
        if (opPlQ.bIsRightAnswer)
        {
            opRights++;
        }
    }
    
    NSInteger isMyWin = 0;
    BOOL resultByTime = NO;
    
    if (myRights > opRights)
    {
        isMyWin = 1;
        //        myCoins += 5 * myRights;
        myCoins = bid;
    }
    else if (myRights < opRights)
    {
        isMyWin = 0;
        //        myCoins -= 5 * 5;
        myCoins -= bid;
    }
    else // ==
    {
        
        if (myTotalTime < opTotalTime)
        {
            isMyWin = 1;
            resultByTime = YES;
            //            myCoins += 5 * myRights;
            myCoins = bid;
        }
        else if (myTotalTime > opTotalTime)
        {
            isMyWin = 0;
            resultByTime = YES;
            //            myCoins -= 5 * 5;
            myCoins -= bid;
        }
        else // ==
        {
            isMyWin = 3;
            //            myCoins += 5 * myRights;
            if (myRights != 0)
                myCoins = bid;
        }
    }
    myPoints = 0;
    for (int i = 0; i < 5; i++)
    {
        PlayingQuestion *myPlQ = [myQuestions objectForKey:[NSNumber numberWithInt:i]];
        if (!myPlQ)
        {
            myPlQ = [PlayingQuestion playingQuestionWithId:i answerTime:[[GameModel sharedInstance] questionTime] isRight:NO];
        }
        
        PlayingQuestion *opPlQ = [opQuestions objectForKey:[NSNumber numberWithInt:i]];
        if (!opPlQ)
        {
            opPlQ = [PlayingQuestion playingQuestionWithId:i answerTime:[[GameModel sharedInstance] questionTime] isRight:NO];
        }
        
        if (myPlQ.bIsRightAnswer)
        {
            if (isMyWin) {
                myPoints += 5;
            } else {
                myPoints += 1;
            }
        }
    }
    
    if (isMyWin == 1)
    {
        myPoints *= 2;
    }
    
    [self personGetCoins:myCoins];
//    [self personGetPoints:myPoints];
    
    [self saveBotGameResult:isMyWin];
    
    //    _person.earnedPoints = [NSNumber numberWithInteger:[_person.earnedPoints integerValue] + myCoins];
    //    _person.points = [NSNumber numberWithInteger:[_person.points integerValue] + myPoints];
    
    //    [DELEGATE saveContext];
    
    NSDictionary *res = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:myCoins], @"myCoins", [NSNumber numberWithInteger:myPoints],@"myPoints", [NSNumber numberWithInteger:isMyWin], @"isMyWin",[NSNumber numberWithBool:resultByTime], @"isByTime", nil];
    
    return res;
    
}

-(BOOL)personSpentCoins:(NSInteger)coins
{
    if ([_person.allPersonPoints integerValue] >= coins)
    {
        
        NSInteger earnedPoints = [_person.earnedPoints integerValue];
        NSInteger boughtPoints = [_person.boughtPoints integerValue];
        NSInteger tapPoints = [_person.tapjoyedPoints integerValue];
        NSInteger nadoOtnat = coins;
        if (earnedPoints > 0 && earnedPoints < coins)
        {
            nadoOtnat = coins - earnedPoints;
            earnedPoints = 0;
        }
        
        if (earnedPoints >= coins)
        {
            earnedPoints -= coins;
        }
        else if (tapPoints >= nadoOtnat)
        {
            tapPoints -= nadoOtnat;
            
            [Tapjoy spendCurrency:coins];
//            [Tapjoy spendTapPoints:(int)coins];
        }
        else if (boughtPoints >= nadoOtnat)
        {
            boughtPoints -= nadoOtnat;
        }
        _person.earnedPoints = [NSNumber numberWithInteger:earnedPoints];
        _person.boughtPoints = [NSNumber numberWithInteger:boughtPoints];
        _person.tapjoyedPoints = [NSNumber numberWithInteger:tapPoints];
        
        [DELEGATE saveContext];
        
        
        return YES;
        
    }
    
    return NO;
    
}

-(void)personGetCoins:(NSInteger)coins
{
    _person.earnedPoints = [NSNumber numberWithInteger:[_person.earnedPoints integerValue] + coins];
    [DELEGATE saveContext];
}

-(void)personGetPoints:(NSInteger)points
{
    _person.points = [NSNumber numberWithInteger:[_person.points integerValue] + points];
    
    [DELEGATE saveContext];
    
    [[GameCenterManager sharedInstance] reportScore:[_person.points integerValue] forCategory:kLeaderboardID];
    
}

-(void)personGetPointsOffline:(NSInteger)points
{
    _person.points = [NSNumber numberWithInteger:[_person.points integerValue] + points];
    
    [DELEGATE saveContext];
    
    [[GameCenterManager sharedInstance] reportScore:[_person.points integerValue] forCategory:[[GameModel sharedInstance] leaderboardCategory]];
    
}

-(void)saveBotGameResult:(NSInteger)result
{
    if (result == 0)
    {
        _person.botWins = [NSNumber numberWithInteger:[_person.botWins integerValue] + 1];
    }
    else if (result == 1)
    {
        _person.botLoses = [NSNumber numberWithInteger:[_person.botLoses integerValue] + 1];
    }
    else if (result == 3)
        _person.botDraws = [NSNumber numberWithInteger:[_person.botDraws integerValue] + 1];
    
    [DELEGATE saveContext];
}
@end
