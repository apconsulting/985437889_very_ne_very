//
//  Person.h
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CrManagedObject.h"


@interface Person : CrManagedObject

@property (nonatomic, retain) NSNumber * boughtPoints; //boughtPoints
@property (nonatomic, retain) NSNumber * earnedPoints; //earnedPoints
@property (nonatomic, retain) NSNumber * points;
@property (nonatomic, retain) NSNumber * questionIndex1;
@property (nonatomic, retain) NSNumber * questionIndex2;
@property (nonatomic, retain) NSNumber * questionIndex3;
@property (nonatomic, retain) NSNumber * tapjoyedPoints; //tapjoyedPoints
@property (nonatomic, retain) NSNumber * botDraws;
@property (nonatomic, retain) NSNumber * botLoses;
@property (nonatomic, retain) NSNumber * botWins;

-(NSInteger)numberOfMatches;

-(NSNumber*)allPersonPoints;

@end
