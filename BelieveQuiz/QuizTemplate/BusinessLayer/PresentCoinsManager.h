//
//  PresentCoinsManager.h
//  AudioQuiz
//
//  Created by Vladislav on 5/29/13.
//  Copyright (c) 2013 Vladislav. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PresentCoinsManager : NSObject

+ (BOOL)appLaunch;

@end
