//
//  StavkaViewController.m
//  GuessTheCar2
//
//  Created by Uladzislau Yasnitski on 20/11/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "StavkaViewController.h"
#import "Localization.h"
#import "UIFont+CustomFont.h"
#import "UIColor+NewColor.h"
#import "ConstantsAndMacros.h"

#define minBid 25
#define medBid 50
#define higBid 75
#define vipBid 100

@interface StavkaViewController ()
{
    NSInteger maxBid;
}
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btn25;
@property (weak, nonatomic) IBOutlet UIButton *btn50;
@property (weak, nonatomic) IBOutlet UIButton *btn75;
@property (weak, nonatomic) IBOutlet UIButton *btn100;
@property (strong, nonatomic) NSArray *buttons;
@end

@implementation StavkaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil withMaxBit:(NSInteger)bid
{
    if (self = [self initWithNibName:nibNameOrNil bundle:nil])
    {
        // Custom initialization
     
        maxBid = bid;
    }
    return self;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil buttons:(NSArray*)buttons {
    if (self = [self initWithNibName:nibNameOrNil bundle:nil])
    {
        // Custom initialization

        self.buttons = buttons;
    }
    return self;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    CGFloat fontSize = isPad ? 40 : 25;
    if (self.buttons) {
        _lbTitle.text = @"ВКонтакте";
    } else {
        _lbTitle.text = [[Localization instance] stringWithKey:@"txt_makeBid"];
    }
    _lbTitle.textAlignment = NSTextAlignmentCenter;
    _lbTitle.textColor = [UIColor darkGrayColor];
    _lbTitle.font = [UIFont myFontSize:fontSize];
    _lbTitle.adjustsFontSizeToFitWidth = YES;
    
    
    if (self.buttons) {
        [self configLabel:_btn25 withText:self.buttons[0] bid:vkAuthErnedCoins];
        _btn25.tag = 0;
        [self configLabel:_btn50 withText:self.buttons[1] bid:vkSibscribeErnedCoins];
        _btn50.tag = 1;
        [self configLabel:_btn75 withText:self.buttons[2] bid:vkShareErnedCoins];
        _btn75.tag = 2;
        [self configLabel:_btn100 withText:self.buttons[3] bid:vkInviteFriendErnedCoins];
        _btn100.tag = 3;
   } else {
        [self configLabel:_btn25 withText:[[Localization instance] stringWithKey:@"txt_makeMinimum"] bid:minBid];
        [self configLabel:_btn50 withText:[[Localization instance] stringWithKey:@"txt_makeMedium"] bid:medBid];
        [self configLabel:_btn75 withText:[[Localization instance] stringWithKey:@"txt_makeHigh"] bid:higBid];
        [self configLabel:_btn100 withText:[[Localization instance] stringWithKey:@"txt_makeVip"] bid:vipBid];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)configLabel:(UIButton*)btn withText:(NSString*)text bid:(NSInteger)bid
{
    CGFloat fontSize = isPad ? 28 : 18;
    if (!self.buttons) {
        [btn setUserInteractionEnabled:maxBid < bid ? NO : YES];
        [btn setTitleColor:(maxBid < bid ? [UIColor grayColor] : [UIColor whiteColor]) forState:UIControlStateNormal];
        btn.tag = bid;
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    } else {
        fontSize -= 0;
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    [btn setTitle:text forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont myFontSize:fontSize]];

}

- (IBAction)didStavka:(UIButton *)sender {
    if (self.buttons) {
        [_delegate vkOptionChoosen:sender.tag];
        return;
    }
    [_delegate stavkaViewDidBid:sender.tag];
}

- (IBAction)didClose:(id)sender {
    [_delegate stavkaViewDidClose];
}

@end
