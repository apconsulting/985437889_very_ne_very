//
//  GetCoinsViewController.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 14/11/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "GetCoinsViewController.h"
#import "MKStoreManager.h"
#import "MBProgressHUD.h"
#import "UIViewController+MJPopupViewController.h"
#import "TapJoyViewController.h"
#import "AppSettings.h"
#import "Localization.h"
#import "UIFont+CustomFont.h"
#import "Person.h"
#import "BaseViewController.h"

@interface GetCoinsViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *imgFon;
@property (weak, nonatomic) IBOutlet UIButton *btn_300;
@property (weak, nonatomic) IBOutlet UIButton *btn_750;
@property (weak, nonatomic) IBOutlet UIButton *btn_1800;
@property (weak, nonatomic) IBOutlet UIButton *btn_3000;
@property (weak, nonatomic) IBOutlet UIButton *btnTapJoy;
@property (nonatomic, weak) IBOutlet UIButton *btnRestore;
@property (nonatomic, weak) IBOutlet UILabel *restore;
@property (nonatomic, weak) IBOutlet UILabel *restoreSub;
- (IBAction)didDismiss:(id)sender;
@end

@implementation GetCoinsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _imgFon.image = [UIImage imageNamed:[NSString stringWithFormat:@"IAP_%@.jpg", [AppSettings currentLanguage]]];

    _restore.text = [[Localization instance] stringWithKey:@"txt_restoreAds"];
    _restore.font = [UIFont myFontSize:isPad ? 24 : 12];
    _restore.textColor = [UIColor whiteColor];
    _restore.textAlignment = NSTextAlignmentCenter;
    
    _restoreSub.text = [[Localization instance] stringWithKey:@"txt_ads"];
    _restoreSub.font = [UIFont myFontSize:isPad ? 20 : 10];
    _restoreSub.textColor = [UIColor whiteColor];
    _restoreSub.textAlignment = NSTextAlignmentCenter;
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    UIViewController *top = DELEGATE.navigationController.topViewController;
    if ([[top class] isSubclassOfClass:[BaseViewController class]])
    {
        BaseViewController *vc = (BaseViewController*)top;
        [vc updateCoinsForLabel:vc.lbCoins withAnimation:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)did_300:(id)sender {
    [self purchaseForItem:APP_DONATE_300_POINTS];
}

- (IBAction)did_750:(id)sender {
    [self purchaseForItem:APP_DONATE_750_POINTS];
}
- (IBAction)did_1800:(id)sender {
    [self purchaseForItem:APP_DONATE_1800_POINTS];
}
- (IBAction)did_3000:(id)sender {
    [self purchaseForItem:APP_DONATE_3000_POINTS];
}
- (IBAction)didTapJoy:(id)sender {
    [self.delegate getCoinsDidVideo];
//    TapJoyViewController *vc = [[TapJoyViewController alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)didRestore
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[MKStoreManager sharedManager] restorePreviousTransactionsOnComplete:^{
       
        [self feyturePurshased:nil];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    } onError:^(NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

-(void)feyturePurshased:(NSString*)feature
{
    NSInteger coins = 0;
    if ([feature isEqualToString:APP_DONATE_300_POINTS])
        coins = 300;
    else if ([feature isEqualToString:APP_DONATE_750_POINTS])
        coins = 750;
    else if ([feature isEqualToString:APP_DONATE_1800_POINTS])
        coins = 1800;
    else if ([feature isEqualToString:APP_DONATE_3000_POINTS])
        coins = 3000;
    else if ([feature isEqualToString:APP_REMOVE_ADS])
    {
        UIViewController *top = DELEGATE.navigationController.topViewController;
        if ([[top class] isSubclassOfClass:[BaseViewController class]])
        {
            BaseViewController *vc = (BaseViewController*)top;
            [vc removeBanner];
        }
    }
    Person *p = [[Person allObjects] lastObject];
    p.boughtPoints = [NSNumber numberWithInteger:[p.boughtPoints integerValue] + coins];
    [DELEGATE saveContext];
    
    [self didDismiss:nil];
}

-(void)purchaseForItem:(NSString*)str
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[MKStoreManager sharedManager] buyFeature:str onComplete:^(NSString *purchasedFeature, NSData *purchasedReceipt, NSArray *availableDownloads) {
    
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        [self feyturePurshased:purchasedFeature];
        
    } onCancelled:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}
- (IBAction)didDismiss:(id)sender {
    [_delegate getCoinsDismissed];
}
@end
