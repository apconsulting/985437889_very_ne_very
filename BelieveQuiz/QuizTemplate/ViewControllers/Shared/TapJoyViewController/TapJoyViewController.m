//
//  TapJoyViewController.m
//  AudioQuiz
//
//  Created by Vladislav on 4/10/13.
//  Copyright (c) 2013 Vladislav. All rights reserved.
//

#import "TapJoyViewController.h"
#import <Tapjoy/Tapjoy.h>
#import "MBProgressHUD.h"
#import "Localization.h"
#import "AppSettings.h"
#import "DEMOSecondViewController.h"
#import "ConstantsAndMacros.h"
#import <Chartboost/Chartboost.h>
#import "AdToAppSDK.h"
#import "Person.h"
#import <UnityAds/UnityAds.h>
#import "UIViewController+MJPopupViewController.h"


@interface TapJoyViewController ()<ChartboostDelegate, AdToAppSDKDelegate, UnityAdsDelegate, TJPlacementDelegate> {
    BOOL _shouldShowAdd;
}

@end

@implementation TapJoyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
            
        }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:[[Localization instance] stringWithKey:@"txt_loadSavePoint"] style:UIBarButtonItemStyleBordered target:self action:@selector(didUpdatePoints)];
    [right setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]} forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = right;
    right = nil;
    
    // Back Button
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithTitle:[[Localization instance] stringWithKey:@"txt_back"] style:UIBarButtonItemStylePlain target:self action:@selector(didBack)];
    self.navigationItem.leftBarButtonItem = left;
    [left setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor]} forState:UIControlStateNormal];
    
    left = nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(offerwallClosed:)
                                                 name:TJC_VIEW_CLOSED_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUpdatedPoints:) name:TJC_GET_CURRENCY_RESPONSE_NOTIFICATION object:nil];
    _shouldShowAdd = YES;
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"isSubscribe"]){
        return;
    }
    if (![[AppSettings currentLanguage] isEqualToString:@"ru_RU"]){
        return;
    }
    
    
//    _shouldShowAdd = NO;
//    DEMOSecondViewController* vcModalSub = [[DEMOSecondViewController alloc] init];
//    vcModalSub.view.backgroundColor =  isPad?colorBackgroundSubsribeForIpad:[UIColor whiteColor];
//    [self.navigationController presentViewController:vcModalSub animated:YES completion:^{
//        _shouldShowAdd = YES;
//    }];
}

- (void)loadAd {
    NSDate *today = [NSDate date];
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:NSDayCalendarUnit fromDate:today];
    BOOL even = components.day % 2;
    if (!even) {
        [self showChartboost];
    } else {
        [self showUnity];
    }
}

- (void)showUnity {
    [[UnityAds sharedInstance] setViewController:self.navigationController];
    [[UnityAds sharedInstance] setDelegate:self];
    [[UnityAds sharedInstance] setZone:@"rewardedVideo"];

    if ([[UnityAds sharedInstance] canShow]) {
        // If both are ready, show the ad.
        [[UnityAds sharedInstance] show];
    } else {
        NSLog(@"UnityAds cant be shown");
        [self showTapjoy];
    }
}

- (void)showChartboost {
    [Chartboost startWithAppId:ChartboostAppId appSignature:ChartboostSignature delegate:self];
    [Chartboost showRewardedVideo:CBLocationMainMenu];
}

- (void)showAdToApp {
    [AdToAppSDK setDelegate:self];
    [AdToAppSDK showInterstitial:ADTOAPP_REWARDED_INTERSTITIAL];//ADTOAPP_VIDEO_INTERSTITIAL];
    [self performSelector:@selector(checkATAIntersistal) withObject:nil afterDelay:3];
}

- (void)checkATAIntersistal {
    if (![AdToAppSDK isInterstitialDisplayed]) {
        [self showUnity];
    }
}

- (void)showTapjoy {
    TJPlacement *placement = [TJPlacement placementWithName:TapJoyPlace delegate:self ];
    if (placement.isContentReady) {
        [placement showContentWithViewController: self.navigationController];
    } else {
        [placement requestContent];
    }
}

#pragma mark -
- (void)requestDidSucceed:(TJPlacement*)placement{
    NSLog(@"Tapjoy request success");
}

- (void)requestDidFail:(TJPlacement*)placement error:(NSError*)error{
    NSLog(@"Tapjoy error %@", error);
}

- (void)contentIsReady:(TJPlacement*)placement{
    [placement showContentWithViewController: self.navigationController];
} //This is called when the content is actually available to display.

#pragma mark UnityAdsDelegate
- (void)unityAdsVideoCompleted:(NSString *)rewardItemKey skipped:(BOOL)skipped {
    if (!skipped) {
        [self didRewardPoints:unityReward];
    }
}

#pragma mark AdToAppSDKDelegate
- (void)onAdDidDisappear:(NSString*)adType {
    [self didRewardPoints:0];
}

- (void)onReward:(int)reward currency:(NSString*)gameCurrency {
    [self didRewardPoints:reward];
}

- (void)onAdWillAppear:(NSString*)adType {
    NSLog(@"On AdToApp Interstitial Show");
}

#pragma mark ChartboostDelegate
- (void)didFailToLoadRewardedVideo:(CBLocation)location
                         withError:(CBLoadError)error {
    [self showUnity];
}

- (void)didCompleteRewardedVideo:(CBLocation)location withReward:(int)reward {
    [self didRewardPoints:reward * chartboostReward];
}

- (void)didCloseRewardedVideo:(CBLocation)location {
    [self didRewardPoints:0];
}

- (void)didRewardPoints:(NSInteger)points {
    if (points) {
        Person *person = [[Person allObjects] lastObject];
        NSInteger earnedPoints = [person.earnedPoints integerValue];
        earnedPoints += points;
        person.earnedPoints = @(earnedPoints);

        [DELEGATE saveContext];
    }
    [self.navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
}

-(void)didUpdatePoints
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [Tapjoy getCurrencyBalance];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    if (_shouldShowAdd) {
        self.view.backgroundColor = UIColor.lightGrayColor;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    if (_shouldShowAdd) {
        _shouldShowAdd = NO;
        self.view.backgroundColor = UIColor.lightGrayColor;
        [self loadAd];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getUpdatedPoints:(NSNotification*)notifyObj
{

    [self didBack];
}

-(void)offerwallClosed:(NSNotification*)notifyObj
{
	NSLog(@"Offerwall closed");
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    [self performSelector:@selector(didBack) withObject:nil afterDelay:2];
    
}

-(void)didBack
{
    [Tapjoy getCurrencyBalance];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.navigationController dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];

//    [self.navigationController popViewControllerAnimated:YES];
//    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)dealloc
{
    
//    [super dealloc];
}

#pragma mark Tapjoy Display Ads Delegate Methods

- (void)didReceiveAd:(TJCAdView*)adView
{
	NSLog(@"Tapjoy Display Ad Received");
}


- (void)didFailWithMessage:(NSString*)msg
{
	NSLog(@"No Tapjoy Display Ads available");
}

- (BOOL)shouldRefreshAd
{
	return YES;
}

#pragma mark Tapjoy View Delegate Methods

- (void)viewWillAppearWithType:(int)viewType
{
	NSLog(@"Tapjoy viewWillAppearWithType: %d", viewType);
}


- (void)viewDidAppearWithType:(int)viewType
{
	NSLog(@"Tapjoy viewDidAppearWithType: %d", viewType);
}


- (void)viewWillDisappearWithType:(int)viewType
{
	NSLog(@"Tapjoy viewWillDisappearWithType: %d", viewType);
}


- (void)viewDidDisappearWithType:(int)viewType
{
	NSLog(@"Tapjoy viewDidDisappearWithType: %d", viewType);
}
@end
