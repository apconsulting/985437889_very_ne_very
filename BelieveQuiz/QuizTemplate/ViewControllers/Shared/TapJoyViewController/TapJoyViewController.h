//
//  TapJoyViewController.h
//  AudioQuiz
//
//  Created by Vladislav on 4/10/13.
//  Copyright (c) 2013 Vladislav. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Tapjoy/Tapjoy.h>


@interface TapJoyViewController : UIViewController <TJCVideoAdDelegate>

@end
