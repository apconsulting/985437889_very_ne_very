//
//  MainViewController.h
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 11/12/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "BaseViewController.h"
#import <GameKit/GameKit.h>
#import "GameCenterManager.h"
#import "StavkaViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface MainViewController : BaseViewController <UIAlertViewDelegate,GKLeaderboardViewControllerDelegate,FBSDKSharing, StavkaViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnStartGame;
@property (weak, nonatomic) IBOutlet UIButton *btnStartOnlineGame;
@property (weak, nonatomic) IBOutlet UIButton *btnLeaderBoard;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveAds;
@property (weak, nonatomic) IBOutlet UIButton *btnDidCoins;
@property (weak, nonatomic) IBOutlet UIButton *btnInfo;
@property (weak, nonatomic) IBOutlet UILabel *lbStartGame;
@property (weak, nonatomic) IBOutlet UILabel *lbStartOnline;
@property (weak, nonatomic) IBOutlet UIButton *btnMoreApp;
@property (weak, nonatomic) IBOutlet UIButton *levelButton;



@property (nonatomic) NSInteger playerBid;

- (IBAction)showLevelActionSheet;
- (IBAction)didStartGame:(id)sender;
- (IBAction)didStartOnlineGame:(id)sender;
- (IBAction)didLeaderBoard:(id)sender;
- (IBAction)didRemoveAds:(id)sender;
- (IBAction)didInfo:(id)sender;

- (IBAction)didLink:(id)sender;
@end
