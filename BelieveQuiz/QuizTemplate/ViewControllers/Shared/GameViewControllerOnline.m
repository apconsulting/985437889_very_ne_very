//
//  GameViewControllerOnliner.m
//  QuizTemplate
//
//  Created by Uladzislau Yasnitski on 15/11/13.
//  Copyright (c) 2013 Uladzislau Yasnitski. All rights reserved.
//

#import "GameViewControllerOnline.h"

@interface GameViewControllerOnline ()
{
    NSTimer *questionTimer;
    NSInteger timerValue;
    MBProgressHUD *HUD;
}
@end

@implementation GameViewControllerOnline

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil questionIds:(NSString*)str;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[GameModel sharedInstance] startWithQuestionIds:str];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(enterBackground)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameModelReceivedData:) name:@"GameModelReceivedData" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gameModelMatchEnded:) name:@"GameModelMatchEnded" object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGFloat topPadding=0;
    CGFloat bottomPadding =0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    CGRect playersViewFrame = _playersView.frame;
//    playersViewFrame.size.width = 375;
    _playersView.translatesAutoresizingMaskIntoConstraints = NO;
    playersViewFrame.origin.y =topPadding + self.imageTopView.frame.origin.y + self.imageTopView.frame.size.height;
    _playersView.frame = playersViewFrame;
//    _playersView.backgroundColor = [UIColor redColor];
    [_playersView shoewView];
    

    
    [self.view addSubview:_playersView];
    
    [[GameModel sharedInstance] onlineGameStarted];
    
    self.btnQuestionIndex.userInteractionEnabled = NO;
    self.btntopCoins.userInteractionEnabled = NO;
    [self showQuestion];
}

-(void)onBack
{
    [self releaseTimer];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[GameModel sharedInstance]finishOnlineGame];
}

-(void)enterBackground
{
    [HUD removeFromSuperview];
    HUD = nil;
    [self onBack];
}

-(void)releaseTimer
{
	[questionTimer invalidate];
	questionTimer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 123)
        [self onBack];
}

-(void)showQuestion
{
    [self.questionView removeFromSuperview];
    self.questionView = nil;
    
    [self.btnQuestionIndex setTitle:[NSString stringWithFormat:@"%u/5",[GameModel sharedInstance].questionIndex + 1] forState:UIControlStateNormal];
    
    
    if (![[Model instance] isInternetAvailable])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:[[Localization instance] stringWithKey:@"txt_error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.tag = 123;
        [alert show];
    }
    
    
    QuestionNew *qe = [GameModel sharedInstance].question;
    if (!qe)
    {
        [self doVictoryGame];
        return;
    }
    
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    CGFloat screenHeight = screenSize.height;
    
    CGFloat topPadding=0;
    CGFloat bottomPadding =0;
    if (@available(iOS 11.0, *)) {
        UIWindow *window = UIApplication.sharedApplication.keyWindow;
        topPadding = window.safeAreaInsets.top;
        bottomPadding = window.safeAreaInsets.bottom;
    }
    
    CGRect qFr;
    qFr.origin.x = 0;
    CGFloat ssss = _playersView.frame.size.height;
//    qFr.origin.y = self.imageTopView.frame.origin.y + self.imageTopView.frame.size.height + _playersView.frame.size.height+topPadding;
    qFr.origin.y = _playersView.frame.origin.y + _playersView.frame.size.height;
    qFr.size.width =screenWidth;// self.view.frame.size.width;
    qFr.size.height = screenHeight - self.admobBannerView.frame.size.height - qFr.origin.y-bottomPadding;

    self.questionView = [[QuestionView alloc] initWithDelegate:self question:qe isOnline:YES h:qFr.size.height];
    self.questionView.frame = qFr;

    [self.view addSubview:self.questionView];
    [self releaseTimer];
    [self hideWaitOpponent];
    
    timerValue = TimerValue;
    
    questionTimer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(onTimer:) userInfo:nil repeats:YES];

}

-(void)onTimer:(NSTimer*)timer
{
    --timerValue;

    NSLog(@"timer Value - %lu",(unsigned long)timerValue);

    [_playersView updateMyProgressForQuestionIndex:[GameModel sharedInstance].questionIndex withValue:timerValue];
    if (timerValue <= 0)
    {
        NSLog(@"Timer is OVEr");
        [self didForWrongAnswer];

    }
}

-(void)showWaitOpponentLabel
{
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.labelText = [[Localization instance] stringWithKey:@"txt_wait"];
    [HUD show:YES];
    
    [GameModel sharedInstance].playingPerson.isPlayerIsWaiting = YES;
}

-(void)hideWaitOpponent
{
    [HUD removeFromSuperview];
    HUD = nil;
    [GameModel sharedInstance].playingPerson.isPlayerIsWaiting = NO;
}

-(BOOL)isCurrentStateIsWaitingForOpponent
{
    return [GameModel sharedInstance].playingPerson.isPlayerIsWaiting;
}
-(void)doVictoryGame
{
    if (!self.questionView)
        return;
  
    [self.questionView removeFromSuperview];
    self.questionView = nil;
    
    [self hideWaitOpponent];
        
    NSDictionary *result = [[GameModel sharedInstance] calculateResultWithBid:_playerBid];
    
//    NSInteger resCoins = [[result objectForKey:@"myCoins"] integerValue] ;
//    NSInteger resPoitns = [[result objectForKey:@"myPoints"] integerValue] ;
    
    [[GameModel sharedInstance] finishOnlineGame];
    
    WinAlertView *alert = [[WinAlertView alloc] initAlertWithInfo:result];
    alert.delegate = self;
    [self.view addSubview:alert];
}

-(void)didForRightAnswer
{
     [self playRightAnswerSound];
    [self releaseTimer];
    
    [_playersView updateWithMeIsRight:YES forQuestionIndex:[GameModel sharedInstance].questionIndex timerValue:timerValue];
  
    NSDictionary *result = [[GameModel sharedInstance] didForRightQuestionOnlineWithTimerValue:timerValue];
    
    if ([[result objectForKey:@"doTheNextQuestion"] boolValue])
    {
        [self showQuestion];
    }
    else if ([[result objectForKey:@"showWaiting"] boolValue])
    {
        [self showWaitOpponentLabel];
    }
    else if ([[result objectForKey:@"doVictory"] boolValue])
    {
        [self doVictoryGame];
    }
    
//    [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
//    [self updateGameCenterPointsForLabel:self.lbPoints withAnimation:YES];
}

-(void)didForWrongAnswer
{
    [super playWrongSound];
    [self releaseTimer];
    
    [_playersView updateWithMeIsRight:NO forQuestionIndex:[GameModel sharedInstance].questionIndex timerValue:timerValue];
    
    NSDictionary *result = [[GameModel sharedInstance] didForWrongQuestionOnlineWithTimerValue:timerValue];
    
    if ([[result objectForKey:@"doTheNextQuestion"] boolValue])
    {
        [self showQuestion];
    }
    else if ([[result objectForKey:@"showWaiting"] boolValue])
    {
        [self showWaitOpponentLabel];
    }
    else if ([[result objectForKey:@"doVictory"] boolValue])
    {
        [self doVictoryGame];
    }
    
//    [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
//    [self updateGameCenterPointsForLabel:self.lbPoints withAnimation:YES];
    
}

-(void)gameModelReceivedData:(NSNotification*)notif
{
    NSDictionary *result = notif.userInfo;
    
    NSNumber *questionIndex = [result objectForKey:@"questionIndex"];
    BOOL questionIsRight = [[result objectForKey:@"questionIsRight"] boolValue];
    NSNumber *questionTimerValue = [result objectForKey:@"questionTimerValue"];
    BOOL doTheNextQuestion = [[result objectForKey:@"doTheNextQuestion"] boolValue];
    BOOL doVicotry = [[result objectForKey:@"doVictory"] boolValue];
    
    [_playersView updateWithOpponentIsRight:questionIsRight forQuestionIndex:[questionIndex integerValue] timerValue:[questionTimerValue integerValue]];

    if (doTheNextQuestion)
    {
        [self showQuestion];
    }
    else if (doVicotry)
    {
        [self doVictoryGame];
    }
}

-(void)gameModelMatchEnded:(NSNotification*)notif
{
    NSDictionary *result = notif.userInfo;
    BOOL doTheNextQuestion = [[result objectForKey:@"doTheNextQuestion"] boolValue];
   
    if (doTheNextQuestion)
    {
        [self showQuestion];
    }
    else
    {
        [self doVictoryGame];
    }

}

-(void)winAlertClose
{
    [self updateCoinsForLabel:self.lbCoins withAnimation:YES];
    [self updateGameCenterPointsForLabel:self.lbPoints withAnimation:YES];
    
    [self performSelector:@selector(onBack) withObject:nil afterDelay:0.5];
}

@end
