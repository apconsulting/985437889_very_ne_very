//
//  TNetwork.h
//  States
//
//  Created by tinkl on 10/19/16.
//
//

#import <Foundation/Foundation.h>
 
typedef void (^CompletioBlock)(NSDictionary *object, NSError *error);

@interface AVOSCloud : NSObject
//[AVOSCloud setApplicationId:@"x-MdYXbMMI" clientKey:@"x"];

+ (void) setApplicationId:(NSString *)sid clientKey:(NSString*) key;
+ (void) getFirstObjectInBackgroundWithBlock:(CompletioBlock) block;
+ (void) handleRemoteNotificationsWithDeviceToken:(NSData * ) devicetoken;

//不用的类
+(void)setAllLogsEnabled:(BOOL) bol;

@end


@interface AVAnalytics : NSObject
+(void)setChannel:(NSString *) log;
+(void)setAllLogsEnabled:(NSString *) log;
+(void)trackAppOpenedWithLaunchOptions:(id) launchOptions;
+(void)trackAppOpenedWithRemoteNotificationPayload:(id) userInfo;
@end
