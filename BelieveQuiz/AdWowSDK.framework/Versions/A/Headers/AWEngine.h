//
//  AWEngine.h
//  AdWowSDK
//
//  Created by heximal on 30/07/14.
//  Copyright (c) 2014 heximal. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

extern NSString * const AWErrorDomain;
extern NSString * const AWVersion;

@protocol AdWowDelegate;
@class AWNotificationView;
@class AWUnit;
@class AWForm;

typedef enum AWErrorCode {
    AWERR_NetworkError = -1,
    AWERR_UnhandledException = -1000,
    AWERR_OpenSessionFailed = -1001,
    AWERR_CloseSessionFailed = -1002,
    AWERR_RewardUrlFaield = -1003,
    AWERR_InvalidMomentName = -1004,
    AWERR_MomentNameBlacklisted = -1005,
    AWERR_MomentAlreadyLoaded = -1006,
    AWERR_MomentLoading = -1007,
    AWERR_MissingIdentity = -1008
} AWErrorCode;

typedef enum AWGender {
    AWG_Unknown = 0,
    AWG_Male = 1,
    AWG_Female = 2
} AWGender;


/**
 Используйте класс AdWow для интеграции сервиса AdWow со своим приложением
 */
@interface AdWow : NSObject

@property (strong, nonatomic) UITextView * logView;
/** @name Свойства AdWow */
/**
 Свойство определяет, должен ли AdWow SDK синхронизировать графические элементы с ориентацией экрана
 */
@property (assign, nonatomic) BOOL shouldAutoRotate;

/**
 Кастомизированное представление нотификации.
 */
@property (strong, nonatomic) AWNotificationView *notificationView;

/**
 Email пользователя. Установка данного свойства приводит к автозаполнению соотвтетствующего поля в форме награды
 */
@property (strong, nonatomic) NSString *userEmail;

/**
 Пол ползователя. Используется для подбора наиболее релевантных наград
 */
@property (assign, nonatomic) AWGender userGender;

/**
 Дата рождения ползователя. Используется для подбора наиболее релевантных наград
 */
@property (strong, nonatomic) NSDate *userBirthday;

/**
 Имя пользовател ползователя. Используется для подбора наиболее релевантных наград
 */
@property (strong, nonatomic) NSString *userFirstName;

/**
 Фамилия пользователя ползователя. Используется для подбора наиболее релевантных наград
 */

@property (strong, nonatomic) NSString *userSureName;
/**
 Отчество ползователя. Используется для подбора наиболее релевантных наград
 */
@property (strong, nonatomic) NSString *userAlias;


/** @name Геттер и сеттер свойства делегата */

/**
 Делегат объекта AdWow
 
 */
@property (assign, nonatomic, setter = setDelegate:) id<AdWowDelegate> delegate;


/** @name Геттер и сеттер указателя на синглтон AdWow */

/**
 Возвращает указатель на общий указатель AdWow синглтона.
 */
+ (AdWow *) sharedInstance;

/**
 Устанавивает указатель на общий объект AdWow синглтона.
 
 @param adwow новый указатель на экземпляр AdWow.
 */
+ (void) setSharedInstance:(AdWow *)adwow;


/** @name Создание и инициализация объекта AdWow */

/**
 Инициализирует объект AdWow с указанными значениями
 
 @param appKey Ключ приложения.
 @param appSecret Секретный ключ.
 */
- (id) initWithAppKey:(NSString *)appKey andSecret:(NSString *)appSecret;


/** @name Сохранение момента */

/**
 Сохраняет момент.
 
 @param momentName Уникальное имя сохраняемого момента.
 @param handler Блок, вызываемый когда асинхронный HTTP запрос завершается с Модулем и Ошибкой
 Модуль может быть равен nil, если ниодной награды не доступно. Ошибка будет содержать nil, 
 если запрос завершился успешно
 */
- (void) saveMoment:(NSString *)momentName withCompletionHandler:(void (^)(AWUnit *unit, NSError *error))handler;

/**
 Сохраняет момент.
 
 @param momentName Уникальное имя сохраняемого момента.
 @param value Значение момента.
 @param handler Блок, вызываемый когда асинхронный HTTP запрос завершается с Модулем и Ошибкой
 Модуль может быть равен nil, если ниодной награды не доступно. Ошибка будет содержать nil,
 если запрос завершился успешно
 Saves a moment.
 */
- (void) saveMoment:(NSString *)momentName value:(double)value withCompletionHandler:(void (^)(AWUnit *unit, NSError *error))handler;

@end

/**
 Протокол AdWowDelegate определяет методы, которые должен реализовывать класс-делегат AdWow.
 Этот делегат реализует методы обратного вызова для работы с сессиями AdWow
 */
@protocol AdWowDelegate <NSObject>

@optional

/** @name Управление сессиями AdWow */

/**
 Сообщает, что AdWow попытался начать сессию.
 
 @param adWow Указатель на экземпляр AdWow, который произвел сессии.
 @param unit модуль, который доступен для показа после открытия сессии.
 @param error Если не равен nil, то содежит возникшую ошибку.
 */
- (void) adWow:(AdWow *)adWow didStartSessionWithUnit:(AWUnit *)unit error:(NSError *)error;

/**
 Сообщает, что AdWow попытался закрыть сессию.
 
 @param adWow Указатель на экземпляр AdWow, который произвел закрытие сессии.
 @param error Если не равен nil, то содежит возникшую ошибку.
 */
- (void) adWow:(AdWow *)adWow didEndSessionWithError:(NSError *)error;

@end

