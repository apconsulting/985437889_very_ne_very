//
//  AWForm.h
//  AdWowSDK
//
//  Created by heximal on 17/08/14.
//  Copyright (c) 2014 heximal. All rights reserved.
//

#import <Foundation/Foundation.h>

@class AWUnit;

@protocol AWFormDelegate;

/**
 Используйте класс AWForm для отображения полноэкранного модуля AdWow, такого как Награда
 */
@interface AWForm : NSObject

/** @name Свойства AWForm */

/**
 Заголовок диалога загрузки
 */
@property (strong, nonatomic) NSString *title;

/**
 Сообщение диалога загрузки
 */
@property (strong, nonatomic) NSString *message;


/**
 Время жизни формы в секундах. Если в течение данного периода пользователь не совершил никаких действий, форма автоматически скрывается
 */
@property (strong, nonatomic) NSNumber * timeToLive;


/**
 Указатель на родительский экземпляр Модуля
 */
@property (strong, nonatomic) AWUnit *unit;

/** @name Геттеры и сеттеры делегата */

/**
 Делегат объекта Форма.
 */
@property (assign, nonatomic) id<AWFormDelegate> delegate;

@end


/**
 Протокол AWFormDelegate определяет методы делегата класса AWForm.
 Данный делегат реализует методы уведомления такие как показ и скрытие

 */
@protocol AWFormDelegate <NSObject>

/** @name Managing Modals */

@optional

/**
 Сообщает делегату, что Форма будет представлена пользователю
 
 @param form Форма, которая будет представлена.
 */
- (void) willPresentForm:(AWForm *)form;

/**
 Сообщает делегату, что Форма скрыта
 
 @param form Форма, которая будет скрыта.
 */
- (void) didDismissForm:(AWForm *)form;



@end
