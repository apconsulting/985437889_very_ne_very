//
//  AWNotification.h
//  AdWowSDK
//
//  Created by heximal on 03/08/14.
//  Copyright (c) 2014 heximal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    AWS_Top = 0,
    AWS_Bottom,
} AWStiky;

@class AWUnit;

@protocol AWNotificationDelegate;

/**
 Используйте класс AWNotification для показа ненавязчивых нотификаций о наградах, выдаваемых пользотвателю
 */
@interface AWNotification : NSObject

/** @name Свойства нотификации */

/**
 Заголовок нотификации.
 */
@property (strong, nonatomic) NSString *title;

/**
 Сообщение нотификации.
 */
@property (strong, nonatomic) NSString *message;

/**
  Положение нотификации на экране
 */
@property (assign, nonatomic) AWStiky sticky;

/**
 Иконка нотификации
 */
@property (strong, nonatomic) UIImage *icon;
/**
  Цвет фона нотификации. По умолчанию светло-серый
 */
@property (strong, nonatomic) UIColor * defaultBackgroundColor;
/**
 Указатель на родительский экземпляр Модуля
 */
@property (strong, nonatomic) AWUnit *unit;


/** @name Сеттер и геттер делегата нотификации */

/**
 Делегат класса AWNotification.
 */
@property (assign, nonatomic) id<AWNotificationDelegate> delegate;

@end


/**
 Протокол AWNotificationDelegate определяет методы делегата класса AWNotification. 
 Данный делегат реализует методы уведомления такие как показ и скрытие
 */
@protocol AWNotificationDelegate <NSObject>

/** @name Управление нотификациями */

@optional



/**
 Сообщает делегату, что Нотификация будет представлена пользователю
 
 @param notification Нотификация, которая будет представлена.
 */
- (void) willPresentNotification:(AWNotification *)notification;

/**
 Сообщает делегату, что Нотификация скрыта
 
 @param notification Нотификация, которая будет скрыта.
 */
- (void) didDismissNotification:(AWNotification *)notification;

/**
 Сообщает делегату, что Нотификация будет скрыта после нажатия пользователем
 
 @param notification Нотификация, которая будет скрыта.
 */
- (void) didDismissNotificationWithClick:(AWNotification *)notification;


@end

