//
//  AWNotificationView.h
//  AdWowSDK
//
//  Created by heximal on 03/08/14.
//  Copyright (c) 2014 heximal. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AWUnit;

/**
 Используйте класс AWNotificationView для кастомизации внешнего вида AWNotification.
 Используйте свойство notificationView в объекте AdWow, чтобы отображать кастомные нотификации пользователю
 */
@interface AWNotificationView : UIButton

/** @name Свойства AWNotificationView */

/**
 Указатель на родительский экземпляр Модуля
 */
@property (strong, nonatomic) AWUnit *unit;

@end



